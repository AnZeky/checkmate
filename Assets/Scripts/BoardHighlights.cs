﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts
{
    public class BoardHighlights : MonoBehaviour
    {
        public GameObject highlightPrefab;
        private List<GameObject> highlights;
        public static BoardHighlights Instance { get; set; }

        private void Start()
        {
            Instance = this;
            highlights = new List<GameObject>();
        }

        private GameObject GetHighlightObject()
        {
            GameObject go = highlights.Find(x => !x.activeSelf);
            if (go == null)
            {
                go = Instantiate(highlightPrefab);
                highlights.Add(go);
            }

            return go;
        }

        public void HighlightAllowedMoves(bool[,] moves)
        {
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    if (moves[i, j])
                    {
                        GameObject go = GetHighlightObject();
                        go.SetActive(true);
                        go.transform.position = new Vector3(i + 0.5f, 0, j + 0.5f);
                    }
                }
            }
        }

        public void HideHighlights()
        {
            foreach (var go in highlights)
            {
                go.SetActive(false);
            }
        }
    }
}