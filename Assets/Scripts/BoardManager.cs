﻿#define GODs

using System.Collections.Generic;
using Assets.Scripts.Pieces;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class BoardManager : MonoBehaviour
    {
        public static BoardManager Instance { get; set; }
        public Chessman[,] Chessmans { get; set; }
        private Chessman selectedChessman;
        private bool[,] allowedMoves { get; set; }

        private const float TILE_SIZE = 1.0f;
        private const float TILE_OFFSET = 0.5f;

        private int selectionX = -1;
        private int selectionY = -1;

        public List<GameObject> chessmanPrefabs;
        private List<GameObject> activeChessman;

        public int[] EnPassantMove;
        private Quaternion orientation = Quaternion.Euler(0, 180, 0);

        public bool isWhiteTurn = true;

        public CanvasGroup alertCanvas;
        private float lastAlert;
        private bool alertActive;

        // Use this for initialization
        private void Start()
        {
            Instance = this;
            SpawnAllChessman();
            Alert("White player's turn");
        }

        // Update is called once per frame
        private void Update()
        {
            DrowChessBoard();
            UpdateSelection();
            UpdateAlert();

            if (Input.GetMouseButtonDown(0))
            {
                if (selectionX >= 0 && selectionY >= 0)
                {
                    if (selectedChessman == null)
                    {
                        // Select chessman
                        SelectChessman(selectionX, selectionY);
                    }
                    else
                    {
                        // Move chessman
                        MoveChessman(selectionX, selectionY);
                    }
                }
            }
        }

        private void SelectChessman(int x, int y)
        {
            if (Chessmans[x, y] == null)
            {
                return;
            }

#if !GOD
            if (Chessmans[x, y].isWhite != isWhiteTurn)
            {
                return;
            }
#endif

            bool hasAtleastOneMove = false;
            allowedMoves = Chessmans[x, y].PossibleMove();
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    if (allowedMoves[i, j])
                    {
                        hasAtleastOneMove = true;
                    }
                }
            }
            if (!hasAtleastOneMove)
            {
                return;
            }

            selectedChessman = Chessmans[x, y];
            BoardHighlights.Instance.HighlightAllowedMoves(allowedMoves);
        }

        private void MoveChessman(int x, int y)
        {
            if (allowedMoves[x, y])
            {
                var c = Chessmans[x, y];

                if (c != null && c.isWhite != isWhiteTurn)
                {
                    // Capture a piece

                    //if it is the king
                    if (c.GetType() == typeof(King))
                    {
                        // end the game
                        EndGame();
                        return;
                    }
                    activeChessman.Remove(c.gameObject);
                    Destroy(c.gameObject);
                }

                if (x == EnPassantMove[0] && y == EnPassantMove[1])
                {
                    if (isWhiteTurn)
                    {
                        c = Chessmans[x, y - 1];
                    }
                    else
                    {
                        c = Chessmans[x, y + 1];
                    }
                    activeChessman.Remove(c.gameObject);
                    Destroy(c.gameObject);
                }

                EnPassantMove[0] = -1;
                EnPassantMove[1] = -1;

                if (selectedChessman.GetType() == typeof(Pawn))
                {
                    if (y == 7)
                    {
                        activeChessman.Remove(selectedChessman.gameObject);
                        Destroy(selectedChessman.gameObject);
                        SpawnChessman(1, x, y, true);
                        selectedChessman = Chessmans[x, y];
                    }
                    if (y == 0)
                    {
                        activeChessman.Remove(selectedChessman.gameObject);
                        Destroy(selectedChessman.gameObject);
                        SpawnChessman(1, x, y, false);
                        selectedChessman = Chessmans[x, y];
                    }

                    if (selectedChessman.CurrentY == 1 && y == 3)
                    {
                        EnPassantMove[0] = x;
                        EnPassantMove[1] = y - 1;
                    }
                    else if (selectedChessman.CurrentY == 6 && y == 4)
                    {
                        EnPassantMove[0] = x;
                        EnPassantMove[1] = y + 1;
                    }
                }

                Chessmans[selectedChessman.CurrentX, selectedChessman.CurrentY] = null;
                selectedChessman.transform.position = GetTileCenter(x, y);
                selectedChessman.SetPosition(x, y);
                Chessmans[x, y] = selectedChessman;
#if !GOD
                isWhiteTurn = !isWhiteTurn;
#endif
            }

            BoardHighlights.Instance.HideHighlights();
            selectedChessman = null;

            Alert(isWhiteTurn ? "White player's turn" : "Black player's turn");
        }

        private void UpdateSelection()
        {
            if (!Camera.main)
                return;

            RaycastHit hit;
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 25.0f, LayerMask.GetMask("ChessPlane")))
            {
                selectionX = (int) hit.point.x;
                selectionY = (int) hit.point.z;
            }
            else
            {
                selectionX = -1;
                selectionY = -1;
            }
        }

        private void SpawnChessman(int index, int x, int y, bool isWhite)
        {
            var go = Instantiate(chessmanPrefabs[index], GetTileCenter(x, y), orientation) as GameObject;
            go.GetComponent<Renderer>().material.mainTexture = Textures.GetTexture(isWhite);
            go.transform.SetParent(transform);
            Chessmans[x, y] = go.GetComponent<Chessman>();
            Chessmans[x, y].SetPosition(x, y);
            Chessmans[x, y].isWhite = isWhite;
            activeChessman.Add(go);
        }

        private void SpawnAllChessman()
        {
            activeChessman = new List<GameObject>();
            Chessmans = new Chessman[8, 8];
            EnPassantMove = new int[2] {-1, -1};

            // Spawn the white team

            // King
            SpawnChessman(0, 3, 0, true);

            //Queen
            SpawnChessman(1, 4, 0, true);

            //Rooks
            SpawnChessman(2, 0, 0, true);
            SpawnChessman(2, 7, 0, true);

            //Bishops
            SpawnChessman(3, 2, 0, true);
            SpawnChessman(3, 5, 0, true);

            //Knights
            SpawnChessman(4, 1, 0, true);
            SpawnChessman(4, 6, 0, true);

            //Pawns
            for (var i = 0; i < 8; i++)
                SpawnChessman(5, i, 1, true);

            // Spawn the Black team

            // King
            SpawnChessman(0, 3, 7, false);

            //Queen
            SpawnChessman(1, 4, 7, false);

            //Rooks
            SpawnChessman(2, 0, 7, false);
            SpawnChessman(2, 7, 7, false);

            //Bishops
            SpawnChessman(3, 2, 7, false);
            SpawnChessman(3, 5, 7, false);

            //Knights
            SpawnChessman(4, 1, 7, false);
            SpawnChessman(4, 6, 7, false);

            //Pawns
            for (var i = 0; i < 8; i++)
            {
                SpawnChessman(5, i, 6, false);
            }
        }

        private Vector3 GetTileCenter(int x, int y)
        {
            var origin = Vector3.zero;
            origin.x += TILE_SIZE*x + TILE_OFFSET;
            origin.z = TILE_SIZE*y + TILE_OFFSET;
            return origin;
        }

        private void DrowChessBoard()
        {
            var widthLine = Vector3.right*8;
            var heigthLine = Vector3.forward*8;
            for (var i = 0; i <= 8; i++)
            {
                var start = Vector3.forward*i;
                Debug.DrawLine(start, start + widthLine);
                for (var j = 0; j <= 8; j++)
                {
                    start = Vector3.right*j;
                    Debug.DrawLine(start, start + heigthLine);
                }
            }

            // Drow the selection
            if ((selectionX >= 0) && (selectionY >= 0))
            {
                Debug.DrawLine(Vector3.forward*selectionY + Vector3.right*selectionX,
                    Vector3.forward*(selectionY + 1) + Vector3.right*(selectionX + 1));
                Debug.DrawLine(Vector3.forward*(selectionY + 1) + Vector3.right*selectionX,
                    Vector3.forward*selectionY + Vector3.right*(selectionX + 1));
            }
        }

        private void EndGame()
        {
            BoardHighlights.Instance.HideHighlights();
            if (isWhiteTurn)
            {
                Alert("White team wins");
                Debug.Log("White team wins");
            }
            else
            {
                Alert("Black team wins");
                Debug.Log("Black team wins");
            }

            foreach (var go in activeChessman)
            {
                Destroy(go);
            }

            isWhiteTurn = true;
            SpawnAllChessman();
        }

        private void Alert(string text)
        {
            alertCanvas.GetComponentInChildren<Text>().text = text;
            alertCanvas.alpha = 1;
            lastAlert = Time.time;
            alertActive = true;
        }

        private void UpdateAlert()
        {
            if (alertActive)
            {
                if (Time.time - lastAlert > 1.5f)
                {
                    alertCanvas.alpha = 1 - (Time.time - lastAlert - 1.5f);

                    if (Time.time - lastAlert > 2.5f)
                    {
                        alertActive = false;
                    }
                }
            }
        }
    }

    public static class Textures
    {
        private static bool _isWhite;
        private static Texture2D _currentTexture;

        static Textures()
        {
            _isWhite = true;
            _currentTexture = (Texture2D) Resources.Load("Textures/White", typeof(Texture2D));
        }

        public static Texture2D GetTexture(bool isWhite)
        {
            if (_isWhite != isWhite)
            {
                _isWhite = isWhite;
                if (isWhite)
                {
                    _currentTexture = (Texture2D) Resources.Load("Textures/White", typeof(Texture2D));
                }
                else
                {
                    _currentTexture = (Texture2D) Resources.Load("Textures/Black", typeof(Texture2D));
                }
            }

            return _currentTexture;
        }
    }
}