﻿//using System;
//using System.IO;
//using System.Net.Sockets;
//using UnityEngine;
//using System.Collections.Generic;
//using UnityEngine.UI;
//
//
//namespace Assets.Scripts
//{
//    public class Client : MonoBehaviour
//    {
//        public string clientName;
//        private bool socketReady;
//        private TcpClient socket;
//
//        private NetworkStream stream;
//        private StreamWriter writer;
//        private StreamReader reader;
//        private List<GameClient> players;
//
//        //        public Text T;
//        //        private void Start()
//        //        {
//        ////            T.text = "";
//        ////            players = new List<GameClient>();
//        //            DontDestroyOnLoad(gameObject);
//        //        }
////        public void ConnectToServer(string host, int port)
////        {
////            DontDestroyOnLoad(gameObject);
////            Network.Connect("127.0.0.1", 2348);
////        }
////
////        public void Disconnect()
////        {
////            Network.Disconnect(250);
////        }
//
//        public bool ConnectToServer(string host, int port)
//        {
//            if (socketReady)
//                return false;
//
//            try
//            {
//                socket = new TcpClient(host, port);
//                stream = socket.GetStream();
//                writer = new StreamWriter(stream);
//                reader = new StreamReader(stream);
//
//                socketReady = true;
//            }
//            catch (Exception e)
//            {
//                Debug.Log("Socket error : " + e.Message);
//            }
//            return true;
//        }
//
//        private void Update()
//        {
//            if (socketReady)
//            {
//                if (stream.DataAvailable)
//                {
//                    string data = reader.ReadLine();
//                    if (data != null)
//                    {
//                        OnIncommingData(data);
//                    }
//                }
//            }
//        }
//
//        // Sending message to the server
//        private void Send(string data)
//        {
//            if (!socketReady)
//                return;
//
//            writer.WriteLine(data);
//            writer.Flush();
//        }
//
//        // Read message from the server
//        private void OnIncommingData(string data)
//        {
////            T.text += "Client: " + data;
//            Debug.Log("Client: " + data);
//            string[] aData = data.Split('|');
//
//            switch (aData[0])
//            {
//                case "SWHO":
//                    for (int i = 1; i < aData.Length; i++)
//                    {
//                        UserConnected(aData[i], false);
//                    }
//                    Send("CWHO|" + clientName);
//                    break;
//
//                case "SCNN":
//                    UserConnected(aData[1], false);
//                    break;
//            }
//        }
//
//        
//                private void UserConnected(string name, bool host)
//                {
//                    GameClient c = new GameClient();
//                    c.name = name;
//        
//                    players.Add(c);
//        
//                }
//        
//                private void OnApplicationQuit()
//                {
//                    CloseSocket();
//                }
//        
//                private void OnDisable()
//                {
//                    CloseSocket();
//                }
//        
//                private void CloseSocket()
//                {
//                    if(!socketReady)
//                        return;
//                    writer.Close();
//                    reader.Close();
//                    stream.Close();
//                    socket.Close();
//                    socketReady = false;
//                }
//        //    }
//
//    }
//    public class GameClient
//    {
//        public string name;
//        public bool isHost;
//    }
//}