﻿//using System;
//using UnityEngine;
//using UnityEngine.Networking;
//using UnityEngine.UI;
//
//namespace Assets.Scripts
//{
//    public class GameManager : NetworkBehaviour
//    {
//        public GameObject clientPrefab;
//        public GameObject connectMenu;
//        public GameObject mainMenu;
//
//        public InputField nameInput;
//        public GameObject serverMenu;
//        public GameObject serverPrefab;
//        public static GameManager Instance { set; get; }
//
////        public Text T;
//        // Use this for initialization
//        private void Start()
//        {
//            Instance = this;
//            serverMenu.SetActive(false);
//            connectMenu.SetActive(false);
//            DontDestroyOnLoad(gameObject);
//        }
//
//        // Update is called once per frame
////        private void Update()
////        {
////            switch (Network.peerType)
////            {
////                case NetworkPeerType.Disconnected:
////
////                    break;
////            }
////        }
//
//        void OnGUI()
//        {
//            if (Network.peerType == NetworkPeerType.Disconnected)
//            {
//                GUI.Label(new Rect(100, 100, 200, 250), "NetworkPeerType.Disconnected");
//            }
//            else if (Network.peerType == NetworkPeerType.Client)
//            {
//                GUI.Label(new Rect(100, 100, 110, 25), "NetworkPeerType.Client");
//            }
//            else if (Network.peerType == NetworkPeerType.Server)
//            {
//                GUI.Label(new Rect(100, 100, 200, 25), "NetworkPeerType.Server");
//                GUI.Label(new Rect(100, 225, 300, 25), "Connections: " + Network.connections.Length);
//            }
//            if (GUI.Button(new Rect(500, 500, 110, 25), "hello"))
//            {
//                GetComponent<NetworkView>().RPC("Hello", RPCMode.All);
//            }
//        }
//
//        public void ConnectButton()
//        {
//            mainMenu.SetActive(false);
//            connectMenu.SetActive(true);
//        }
//
//        public void HostButton()
//        {
//                        try
//                        {
//                            var server = Instantiate(serverPrefab).GetComponent<Server>();
////                            server.T = T;
//                            server.Init();
//            
//                            var client = Instantiate(clientPrefab).GetComponent<Client>();
//                            client.clientName = "ClientOS";
//                            client.ConnectToServer("127.0.0.1", 6321);
//                        }
//                        catch (Exception e)
//                        {
//                            Debug.Log(e.Message);
//                        }
//
//            Network.InitializeServer(1, 2348, false);
//            mainMenu.SetActive(false);
//            serverMenu.SetActive(true);
//        }
//
//        public void ConnectToServerButton()
//        {
//            var hostAddress = GameObject.Find("HostInput").GetComponent<InputField>().text;
//            if (hostAddress == string.Empty)
//                hostAddress = "127.0.0.1";
////            Network.Connect(hostAddress, 6321);
//            connectMenu.SetActive(false);
//            try
//            {
//                var client = Instantiate(clientPrefab).GetComponent<Client>();
//                //                client.T = T;
//                client.clientName = nameInput.text;
//                if (client.clientName == string.Empty)
//                    client.clientName = "Client";
//                client.ConnectToServer(hostAddress, 6321);
//            }
//            catch (Exception e)
//            {
//                Debug.Log(e.Message);
//            }
//        }
//
//        public void BackButton()
//        {
//            mainMenu.SetActive(true);
//            serverMenu.SetActive(false);
//            connectMenu.SetActive(false);
//
//            Network.Disconnect(250);
//
//            //            var server = FindObjectOfType<Server>();
//            //            if (server != null)
//            //            {
//            //                Destroy(server.gameObject);
//            //                server.Disconnect();
//            //            }
//            //
//            //            var client = FindObjectOfType<Client>();
//            //            if (client != null)
//            //            {
//            //                Destroy(client.gameObject);
//            //                client.Disconnect();
//            //            }
//        }
//
//        [RPC]
//        public void CmdHello()
//        {
//            Debug.Log("Hello");
//        }
//    }
//}