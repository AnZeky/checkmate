﻿namespace Assets.Scripts.Pieces
{
    public class Bishop : Chessman
    {
        public override bool[,] PossibleMove()
        {
            var allowed = new bool[8, 8];
            int x, y;

            // Top left
            x = CurrentX;
            y = CurrentY;

            while (true)
            {
                x--;
                y++;
                if (x < 0 || y >= 8)
                {
                    break;
                }

                if (PossibleMove(x, y, ref allowed))
                {
                    break;
                }
            }

            // Top Right
            x = CurrentX;
            y = CurrentY;
            while (true)
            {
                x++;
                y++;
                if (x >= 8 || y >= 8)
                {
                    break;
                }

                if (PossibleMove(x, y, ref allowed))
                {
                    break;
                }
            }

            // Down left
            x = CurrentX;
            y = CurrentY;

            while (true)
            {
                x--;
                y--;
                if (x < 0 || y < 0)
                {
                    break;
                }

                if (PossibleMove(x, y, ref allowed))
                {
                    break;
                }
            }

            // Down right
            x = CurrentX;
            y = CurrentY;

            while (true)
            {
                x++;
                y--;
                if (x >= 8 || y < 0)
                {
                    break;
                }

                if (PossibleMove(x, y, ref allowed))
                {
                    break;
                }
            }

            return allowed;
        }
    }
}