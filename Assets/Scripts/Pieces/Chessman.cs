﻿using System;
using UnityEngine;

namespace Assets.Scripts.Pieces
{
    public abstract class Chessman : MonoBehaviour
    {
        public int CurrentX { get; set; }
        public int CurrentY { get; set; }

        public bool isWhite;

        public void SetPosition(int x, int y)
        {
            CurrentX = x;
            CurrentY = y;
        }

        public virtual bool[,] PossibleMove()
        {
            return new bool[8, 8];
        }

        protected virtual bool PossibleMove(int x, int y, ref bool[,] allowed)
        {
            try
            {
                Chessman chessman = BoardManager.Instance.Chessmans[x, y];
                if (chessman == null)
                {
                    allowed[x, y] = true;
                }
                else
                {
                    if (isWhite != chessman.isWhite)
                    {
                        allowed[x, y] = true;
                    }
                    return true;
                }
                return false;
            }
            catch (IndexOutOfRangeException e)
            {
                Debug.Log(string.Format("X:{0} Y: {1} - {2}", x, y, e.Message));
                throw;
            }
        }
    }
}