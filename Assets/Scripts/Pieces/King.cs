﻿namespace Assets.Scripts.Pieces
{
    public class King : Chessman
    {
        public override bool[,] PossibleMove()
        {
            bool[,] allowed = new bool[8, 8];
            int x, y;

            // Top side
            if (CurrentY != 7)
            {
                x = CurrentX - 1;
                y = CurrentY + 1;
                for (int i = 0; i < 3; i++)
                {
                    if (x >= 0 || x < 8)
                    {
                        PossibleMove(x, y, ref allowed);
                    }
                    x++;
                }
            }

            // Down side
            if (CurrentY != 0)
            {
                x = CurrentX - 1;
                y = CurrentY - 1;
                for (int i = 0; i < 3; i++)
                {
                    if (x >= 0 || x < 8)
                    {
                        PossibleMove(x, y, ref allowed);
                    }
                    x++;
                }
            }

            // Middle left
            if (CurrentY != 0)
            {
                PossibleMove(CurrentX - 1, CurrentY, ref allowed);
            }

            // Middle right
            if (CurrentY != 7)
            {
                PossibleMove(CurrentX + 1, CurrentY, ref allowed);
            }
            return allowed;
        }
    }
}