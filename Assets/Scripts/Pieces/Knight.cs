﻿namespace Assets.Scripts.Pieces
{
    public class Knight : Chessman
    {
        public override bool[,] PossibleMove()
        {
            var allowed = new bool[8, 8];

            // UpLeft
            PossibleMove(CurrentX - 1, CurrentY + 2, ref allowed);

            // UpRight
            PossibleMove(CurrentX + 1, CurrentY + 2, ref allowed);

            // RightUp
            PossibleMove(CurrentX + 2, CurrentY + 1, ref allowed);

            // RightDown
            PossibleMove(CurrentX + 2, CurrentY - 1, ref allowed);

            // DownLeft
            PossibleMove(CurrentX - 1, CurrentY - 2, ref allowed);

            // DownRight
            PossibleMove(CurrentX + 1, CurrentY - 2, ref allowed);

            // LeftUp
            PossibleMove(CurrentX - 2, CurrentY + 1, ref allowed);

            // LeftDown
            PossibleMove(CurrentX - 2, CurrentY - 1, ref allowed);

            return allowed;
        }

        protected override bool PossibleMove(int x, int y, ref bool[,] allowed)
        {
            Chessman chessman;
            if (x >= 0 && x < 8 && y >= 0 && y < 8)
            {
                chessman = BoardManager.Instance.Chessmans[x, y];
                if (chessman == null)
                {
                    allowed[x, y] = true;
                }
                else if (isWhite != chessman.isWhite)
                {
                    allowed[x, y] = true;
                }
            }

            return true;
        }
    }
}