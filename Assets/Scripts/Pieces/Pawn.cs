﻿namespace Assets.Scripts.Pieces
{
    public class Pawn : Chessman
    {
        public override bool[,] PossibleMove()
        {
            bool[,] allowed = new bool[8, 8];
            Chessman c, c2;
            int[] e = BoardManager.Instance.EnPassantMove;

            // White team move
            if (isWhite)
            {
                // Diagonal Left
                if (CurrentX != 0 && CurrentY != 7)
                {
                    if (e[0] == CurrentX - 1 && e[1] == CurrentY + 1)
                    {
                        allowed[CurrentX - 1, CurrentY + 1] = true;
                    }

                    c = BoardManager.Instance.Chessmans[CurrentX - 1, CurrentY + 1];
                    if (c != null && !c.isWhite)
                    {
                        allowed[CurrentX - 1, CurrentY + 1] = true;
                    }
                }

                // Diagonal Right
                if (CurrentX != 7 && CurrentY != 7)
                {
                    if (e[0] == CurrentX + 1 && e[1] == CurrentY + 1)
                    {
                        allowed[CurrentX + 1, CurrentY + 1] = true;
                    }

                    c = BoardManager.Instance.Chessmans[CurrentX + 1, CurrentY + 1];
                    if (c != null && !c.isWhite)
                    {
                        allowed[CurrentX + 1, CurrentY + 1] = true;
                    }
                }

                //Middle
                if (CurrentY != 7)
                {
                    c = BoardManager.Instance.Chessmans[CurrentX, CurrentY + 1];
                    if (c == null)
                    {
                        allowed[CurrentX, CurrentY + 1] = true;
                    }
                }

                // Middle on first move
                if (CurrentY == 1)
                {
                    c = BoardManager.Instance.Chessmans[CurrentX, CurrentY + 1];
                    c2 = BoardManager.Instance.Chessmans[CurrentX, CurrentY + 2];
                    if (c == null && c2 == null)
                    {
                        allowed[CurrentX, CurrentY + 2] = true;
                    }
                }
            }
            else
                // Black team move
            {
                // Diagonal Left
                if (CurrentX != 0 && CurrentY != 0)
                {
                    if (e[0] == CurrentX - 1 && e[1] == CurrentY - 1)
                    {
                        allowed[CurrentX - 1, CurrentY - 1] = true;
                    }

                    c = BoardManager.Instance.Chessmans[CurrentX - 1, CurrentY - 1];
                    if (c != null && c.isWhite)
                    {
                        allowed[CurrentX - 1, CurrentY - 1] = true;
                    }
                }

                // Diagonal Right
                if (CurrentX != 7 && CurrentY != 0)
                {
                    if (e[0] == CurrentX + 1 && e[1] == CurrentY - 1)
                    {
                        allowed[CurrentX + 1, CurrentY - 1] = true;
                    }

                    c = BoardManager.Instance.Chessmans[CurrentX + 1, CurrentY - 1];
                    if (c != null && c.isWhite)
                    {
                        allowed[CurrentX + 1, CurrentY - 1] = true;
                    }
                }

                //Middle
                if (CurrentY != 0)
                {
                    c = BoardManager.Instance.Chessmans[CurrentX, CurrentY - 1];
                    if (c == null)
                    {
                        allowed[CurrentX, CurrentY - 1] = true;
                    }
                }

                // Middle on first move
                if (CurrentY == 6)
                {
                    c = BoardManager.Instance.Chessmans[CurrentX, CurrentY - 1];
                    c2 = BoardManager.Instance.Chessmans[CurrentX, CurrentY - 2];
                    if (c == null && c2 == null)
                    {
                        allowed[CurrentX, CurrentY - 2] = true;
                    }
                }
            }
            return allowed;
        }
    }
}