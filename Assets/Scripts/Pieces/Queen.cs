﻿namespace Assets.Scripts.Pieces
{
    public class Queen : Chessman
    {
        public override bool[,] PossibleMove()
        {
            var allowed = new bool[8, 8];
            int x, y;

            // Right
            x = CurrentX;
            while (true)
            {
                x++;
                if (x >= 8)
                {
                    break;
                }

                if (PossibleMove(x, CurrentY, ref allowed))
                {
                    break;
                }
            }

            // Left
            x = CurrentX;
            while (true)
            {
                x--;
                if (x < 0)
                {
                    break;
                }

                if (PossibleMove(x, CurrentY, ref allowed))
                {
                    break;
                }
            }

            // Up
            y = CurrentY;
            while (true)
            {
                y++;
                if (y >= 8)
                {
                    break;
                }

                if (PossibleMove(CurrentX, y, ref allowed))
                {
                    break;
                }
            }

            // Down
            y = CurrentY;
            while (true)
            {
                y--;
                if (y < 0)
                {
                    break;
                }

                if (PossibleMove(CurrentX, y, ref allowed))
                {
                    break;
                }
            }
        
            // Top left
            x = CurrentX;
            y = CurrentY;

            while (true)
            {
                x--;
                y++;
                if (x < 0 || y >= 8)
                {
                    break;
                }

                if (PossibleMove(x, y, ref allowed))
                {
                    break;
                }
            }

            // Top Right
            x = CurrentX;
            y = CurrentY;
            while (true)
            {
                x++;
                y++;
                if (x >= 8 || y >= 8)
                {
                    break;
                }

                if (PossibleMove(x, y, ref allowed))
                {
                    break;
                }
            }

            // Down left
            x = CurrentX;
            y = CurrentY;

            while (true)
            {
                x--;
                y--;
                if (x < 0 || y < 0)
                {
                    break;
                }

                if (PossibleMove(x, y, ref allowed))
                {
                    break;
                }
            }

            // Down right
            x = CurrentX;
            y = CurrentY;

            while (true)
            {
                x++;
                y--;
                if (x >= 8 || y < 0)
                {
                    break;
                }

                if (PossibleMove(x, y, ref allowed))
                {
                    break;
                }
            }

            return allowed;
        }
    }
}