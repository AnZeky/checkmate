﻿namespace Assets.Scripts.Pieces
{
    public class Rook : Chessman
    {
        public override bool[,] PossibleMove()
        {
            var allowed = new bool[8, 8];

            // Right
            var i = CurrentX;
            while (true)
            {
                i++;
                if (i >= 8)
                {
                    break;
                }

                if (PossibleMove(i, CurrentY, ref allowed))
                {
                    break;
                }
            }

            // Left
            i = CurrentX;
            while (true)
            {
                i--;
                if (i < 0)
                {
                    break;
                }

                if (PossibleMove(i, CurrentY, ref allowed))
                {
                    break;
                }
            }

            // Up
            i = CurrentY;
            while (true)
            {
                i++;
                if (i >= 8)
                {
                    break;
                }

                if (PossibleMove(CurrentX, i, ref allowed))
                {
                    break;
                }
            }

            // Down
            i = CurrentY;
            while (true)
            {
                i--;
                if (i < 0)
                {
                    break;
                }

                if (PossibleMove(CurrentX, i, ref allowed))
                {
                    break;
                }
            }

            return allowed;
        }
    }
}