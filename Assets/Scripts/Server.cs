﻿//using System;
//using System.Collections.Generic;
//using System.IO;
//using System.Net;
//using System.Net.Sockets;
//using UnityEngine;
//using UnityEngine.UI;
//
//namespace Assets.Scripts
//{
//    public class Server : MonoBehaviour
//    {
//        private List<ServerClient> clients;
//        private List<ServerClient> disconnectedList;
//        public int port = 6321;
//
//        private TcpListener server;
//        private bool serverStarted;
//        public Text T;
//
////        public void Init()
////        {
////            DontDestroyOnLoad(gameObject);
//////            Network.InitializeServer(1, 2348, false);
//////            Network.Connect("127.0.0.1", 2348);
////        }
//
//        public void Disconnect()
//        {
////            Network.Disconnect(250);
//        }
//
//        public void Init()
//        {
//            //            T.text = "";
//            DontDestroyOnLoad(gameObject);
//            clients = new List<ServerClient>();
//            disconnectedList = new List<ServerClient>();
//            try
//            {
//                server = new TcpListener(IPAddress.Any, port);
//                server.Start();
//
//                StartLisening();
//                serverStarted = true; 
//            }
//            catch (Exception e)
//            {
//                Debug.Log("Socket error: " + e.Message);
//            }
//        }
//
//        private void Update()
//        {
//            if (!serverStarted)
//                return;
//
//            foreach (var client in clients)
//            {
//                // Is the client still connected
//                if (!IsConnected(client.tcp))
//                {
//                    client.tcp.Close();
//                    disconnectedList.Add(client);
//                    continue;
//                }
//                else
//                {
//                    var networkStream = client.tcp.GetStream();
//                    if (networkStream.DataAvailable)
//                    {
//                        var reader = new StreamReader(networkStream, true);
//                        var data = reader.ReadLine();
//
//                        if (data != null)
//                        {
//                            OnIncommingData(client, data);
//                        }
//                    }
//                }
//            }
//
//            for (var i = 0; i < disconnectedList.Count - 1; i++)
//            {
//                //Tell our player Somebldy has disconnected
//
//                clients.Remove(disconnectedList[i]);
//                disconnectedList.RemoveAt(i);
//            }
//        }
//
//        private void StartLisening()
//        {
//            server.BeginAcceptTcpClient(AcceptTcpClient, server);
//        }
//
//        private void AcceptTcpClient(IAsyncResult ar)
//        {
//            Debug.Log("Somebody has connected to server");
//            string allUsers = "";
//            foreach (var serverClient in clients)
//            {
//                allUsers += serverClient.ClientName + '|';
//            }
//
//            var listener = (TcpListener) ar.AsyncState;
//            var sc = new ServerClient(listener.EndAcceptTcpClient(ar));
//            clients.Add(sc);
//
//            StartLisening();
//
////            Broadcast("SWHO|" + allUsers, clients[clients.Count - 1]);
//        }
//
//        private bool IsConnected(TcpClient c)
//        {
//            try
//            {
//                if (c != null && c.Client != null && c.Client.Connected)
//                {
//                    if (c.Client.Poll(0, SelectMode.SelectRead))
//                    {
//                        return !(c.Client.Receive(new byte[1], SocketFlags.Peek) == 0);
//                    }
//                    return false;
//                }
//                return false;
//            }
//            catch (Exception)
//            {
//                return false;
//            }
//        }
//
//        
//                // Server send
//                private void Broadcast(string data, List<ServerClient> cl)
//                {
//                    foreach (ServerClient serverClient in cl)
//                    {
//                        try
//                        {
//                            StreamWriter writer = new StreamWriter(serverClient.tcp.GetStream());
//                            writer.WriteLine(data);
//                            writer.Flush();
//                        }
//                        catch (Exception e)
//                        {
//                            Debug.Log("Write error : " + e.Message);
//                        }
//                    }
//                }
//        
//                private void Broadcast(string data, ServerClient c)
//                {
//                    Broadcast(data, new List<ServerClient> { c });
//                }
//        
//                // Server read
//                private void OnIncommingData(ServerClient c, string data)
//                {
////                    T.text += "Server: " + data;
//                    Debug.Log("Server: " + c.ClientName + " :: " + data);
////                    string[] aData = data.Split('|');
////        
////                    switch (aData[0])
////                    {
////                        case "CWHO":
////                            c.ClientName = aData[1];
////                            Broadcast("SCNN|" + c.ClientName, clients);
////                            break;
////                    }
//        
//                }
//    }
//
//    public class ServerClient
//    {
//        public string ClientName;
//        public TcpClient tcp;
//
//        public ServerClient(TcpClient tcp)
//        {
//            this.tcp = tcp;
//        }
//    }
//}